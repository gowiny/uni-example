import { createSSRApp } from "vue";
import store from './store'
import router from './router'
import vueClass from '@gowiny/vue-class'
import App from "./App.vue";

export function createApp() {
  const app = createSSRApp(App);
  //添加uni独有的hooks
  app.use(vueClass,{
    hooks:['onInit','onLoad','onShow','onReady','onHide','onUnload','onResize','onPullDownRefresh','onReachBottom','onTabItemTap','onShareAppMessage','onPageScroll','onNavigationBarButtonTap',
  'onBackPress','onNavigationBarSearchInputChanged','onNavigationBarSearchInputConfirmed','onNavigationBarSearchInputClicked','onShareTimeline','onAddToFavorites']})
  app.use(store)
  app.use(router)
  return {
    app,
  };
}
