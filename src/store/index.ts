import { createStore } from 'vuex'


// https://vitejs.dev/guide/features.html#glob-import
const modulesFiles:any = import.meta.globEager('./modules/*.ts')
let modules:any = {}
for (const path in modulesFiles) {
  const moduleName = path.replace(/(.*\/)*([^.]+).*/gi, '$2')
  modules[moduleName] = modulesFiles[path].default
}

const store = createStore({
  state:{
    testName:'我是 testName'
  },
	modules: modules
})
export default store
