import {ActionContext} from 'vuex'
export default {
	namespaced: true,
	state: {
		inited: false,
		navMenu: [],
		active: '',
		appName: '默认应用',
		appId: ''
	},
	getters: {
        isInited(state:any) {
            return state.inited
        }
    },
	mutations: {
		SET_APP_INFO: (state:any, appInfo:{name:string,id:string}) => {
			state.appName = appInfo.name;
			state.appId = appInfo.id;
		},
		SET_INITED: (state:any, inited:boolean) => {
			state.inited = inited;
		},
		SET_NAV_MENU: (state:any, navMenu:any) => {
			state.inited = true
			state.navMenu = navMenu
		},
		TOGGLE_MENU_ACTIVE: (state:any, url:string) => {
			state.active = url
		}
	},
	actions: {
		init({
			commit
		}:ActionContext<any,any>) {
			const result = new Promise((resolve,reject)=>{
				setTimeout(()=>{
					commit('SET_APP_INFO',{
						name:'uniapp测试项目',
						id:'uniapp-test'
					})
					commit('SET_INITED',true)
					resolve(true)
				},2000)
			})
			return result
		},
		setAppInfo({
			commit
		}:ActionContext<any,any>, appInfo:{name:string,id:string}) {
			commit('SET_APP_INFO', appInfo)
		}
	}
}
