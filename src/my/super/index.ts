import {Router,Route} from '@gowiny/uni-router'
import {Vue} from '@gowiny/vue-class'

export class MySuper extends Vue {
    $Router!:Router
    $Route!:Route
    private _MySuper_testData :string="我是 MySuper 的 testData"
    get testData(){
        return this._MySuper_testData
    }
    set testData(val:string){
        this._MySuper_testData = val
    }
    printTestData(){
        console.log(`MySuper.testData=${this._MySuper_testData}`)
    }
}

export class MyComponent extends MySuper{
    private _MyComponent_testData :string="我是 MyComponent 的 testData"
    get testData(){
        return this._MyComponent_testData
    }
    set testData(val:string){
        this._MyComponent_testData = val
    }
    printTestData(){
        super.printTestData()
        console.log(`MyComponent.testData=${this._MyComponent_testData}`)
    }
    created(){
        console.debug(`我是 MyComponent 的 created, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
    mounted(){
        console.debug(`我是 MyComponent 的 mounted, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
}

export class MyPage extends MyComponent{
    private _MyPage_testData :string="我是 MyPage 的 testData"
    get testData(){
        return this._MyPage_testData
    }
    set testData(val:string){
        this._MyPage_testData = val
    }
    printTestData(){
        super.printTestData()
        console.log(`MyPage.testData=${this._MyPage_testData}`)
    }
    onShow(){
        console.debug(`我是 MyPage 的 onShow, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
    onHide(){
        console.debug(`我是 MyPage 的 onHide, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
    created(){
        console.debug(`我是 MyPage 的 created, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
    mounted(){
        super.mounted()
        console.debug(`我是 MyPage 的 mounted, testData: [${this.testData}],super.testData: [${super.testData}]`)
    }
}
