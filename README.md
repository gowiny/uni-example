# gowiny-uni-example

#### 介绍
这是 `gowiny-uni-router` 和 `gowiny-vue-class` 两个项目的示例项目

`gowiny-uni-router`主要演示`beforeEach`和`afterEach`两个路由守卫的使用

`gowiny-vue-class`主要演示了以类的形式编写组件，并演示各种装饰器的使用方法


##### 项目地址
[gowiny-uni-router](https://gitee.com/gowiny/uni-router) => https://gitee.com/gowiny/uni-router

[gowiny-vue-class](https://gitee.com/gowiny/vue-class) => https://gitee.com/gowiny/vue-class

##### 其他说明
`gowiny-vue-class` 可以用于 uniapp + vue3 也可以用于普通的 vue 3 项目


